<?php

require 'Feed.php';


$feed_url = "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_1.xml";
// $feed_url = "http://api.worldbank.org/v2/country/br";

$test_urls = [
    "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_1.xml",
    "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_2.xml",
    "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_3.xml",
    "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_4.xml",
    "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_5.xml",
    "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_6.xml"
];

$feed = new Feed($feed_url);

try {
    echo $feed->toJson();
} catch (Exception $e) {
    echo "Error occured: " . $e->getMessage();
}



?>