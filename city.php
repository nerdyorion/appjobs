<?php
// db-ish, can be moved to separate file ---
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "demo";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT `id`, `title`, `description` FROM `offers` LIMIT 10;";
$result = $conn->query($sql);

$records = [];

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    $records[] = [
        "id" => $row["id"],
        "title" => $row["title"],
        "description" => $row["description"]
    ];
  }
} else {
  echo "No record(s) found.";
}
$conn->close();
//--------------------------------------------

$cities = ["Assisi", "Corsico", "Formigine"];

echo "<pre>";
var_dump(cleanRecords($records, $cities)); // final cleaned result



function cleanRecords($records, $cities) {
    foreach($records as &$data) { // loop through records by reference
        foreach($cities as $city) {
            $tmp = explode(" ", $data["title"]); // break sentence into word array for cleaning
            array_walk($tmp, "cleanWords", $city); // clean words
            $data["title"] =  implode(" ", $tmp); // transfrom cleaned array back to string

            $tmp = explode(" ", $data["description"]); // break sentence into word array for cleaning
            array_walk($tmp, "cleanWords", $city); // clean words
            $data["description"] =  implode(" ", $tmp); // transfrom cleaned array back to string
        }
    }
    return $records;
}

function cleanWords(&$value, $key, $city) {
    if(substr_count($value, $city) > 1) {
        $value = str_replace($city, "", $value); // replace string
    } else {
        // do something else
    }
}



?>