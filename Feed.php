<?php

class Feed {
    private $url;
    private $response;

    public function __construct($url)
    {
        $this->url = $url;
    }

    private function fetch() {
        $this->response = $this->_curl($this->url);
    }

    public function toXML() {
        $output = null;

        if(!$this->response) {
            $this->fetch();
        }

        $output = $this->response;

        header('Content-Type: application/xml; charset=utf-8');
        return $output;
    }

    public function toJson() {
        $output = null;

        if(!$this->response) {
            $this->fetch();
        }

        $response = simplexml_load_string($this->response, "SimpleXMLElement", LIBXML_NOCDATA); // var_dump($response); die;
        $output = $this->xmltojs($response); 

        header('Content-Type: application/json; charset=utf-8');
        return $output;
    }

    public function toArray() {
        $output = null;

        if(!$this->response) {
            $this->fetch();
        }
        
        $response = simplexml_load_string($this->response);
        $json = json_encode($response);
        $output = json_decode($json, TRUE);

        return $output;
    }

    private function _curl($url) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            // CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 360,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        if(!$response){
           die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }
         
        curl_close($ch);
        return $response;
    }

    private function xmltojs($xml) {
        $arrayData = $this->xml2Array($xml);
        return json_encode($arrayData);
    }
    

    public function xml2Array($xml, $options = array()) {
        $defaults = array(
            'namespaceSeparator' => ':',//you may want this to be something other than a colon
            'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),   //array of xml tag names which should always become arrays
            'autoArray' => true,        //only create arrays for tags which appear more than once
            'textContent' => '$',       //key used for the text content of elements
            'autoText' => true,         //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,       //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace
     
        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch']) $attributeName =
                        str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                $attributeKey = $options['attributePrefix']
                        . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                        . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }
     
        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = $this->xml2Array($childXml, $options);
                // list($childTagName, $childProperties) = each($childArray);
                foreach ($childArray as $childTagName => $childProperties) {
                    //replace characters in tag name
                    if ($options['keySearch']) $childTagName =
                            str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                    //add namespace prefix, if any
                    if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
        
                    if (!isset($tagsArray[$childTagName])) {
                        //only entry with this key
                        //test if tags of this type should always be arrays, no matter the element count
                        $tagsArray[$childTagName] =
                                in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                                ? array($childProperties) : $childProperties;
                    } elseif (
                        is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                        === range(0, count($tagsArray[$childTagName]) - 1)
                    ) {
                        //key already exists and is integer indexed array
                        $tagsArray[$childTagName][] = $childProperties;
                    } else {
                        //key exists so convert to integer indexed array with previous value in position 0
                        $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                    }
            }
            }
        }
     
        //get text content of node
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;
     
        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
                ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;
     
        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }
}


?>